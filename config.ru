require 'nephos-server'

#\ -p 8080
use Rack::Reloader, 0
use Rack::ContentLength

app = proc do |env|
  router = Nephos::Router.new
  return router.execute(Rack::Request.new(env))
end

run app
